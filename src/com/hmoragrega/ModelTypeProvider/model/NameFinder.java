package com.hmoragrega.ModelTypeProvider.model;

import com.hmoragrega.ModelTypeProvider.php.ClassFinder;
import com.hmoragrega.ModelTypeProvider.php.ClassParser;

import com.intellij.psi.PsiElement;
import com.jetbrains.php.lang.psi.elements.ClassConstantReference;
import com.jetbrains.php.lang.psi.elements.MethodReference;
import com.jetbrains.php.lang.psi.elements.ParameterList;
import com.jetbrains.php.lang.psi.elements.StringLiteralExpression;

/**
 * Determines the requested Model in a Model instantiator method
 */
public class NameFinder {

    private ClassFinder classFinder;
    private ClassParser classParser;

    /**
     * Constructor
     *
     * @param classFinder Dependency for finding classes
     * @param classParser Dependency for parsing classes
     */
    public NameFinder(ClassFinder classFinder, ClassParser classParser) {
        this.classFinder = classFinder;
        this.classParser = classParser;
    }

    /**
     * Get the requested Model path in an instantiator method
     *
     * @param methodReference The method reference
     * @return The model path
     * @throws Exception
     */
    public String getModelPath(MethodReference methodReference) throws Exception {
        ParameterList parameterList = methodReference.getParameterList();
        if (null == parameterList) {
            throw new Exception("No parameters for model factory method");
        }

        PsiElement modelElement = parameterList.getFirstChild();
        if (null == modelElement) {
            throw new Exception("Invalid argument for factory method");
        }

        if (modelElement instanceof ClassConstantReference) {
            return classParser.getClassConstantValue((ClassConstantReference) modelElement, classFinder);
        } else if (modelElement instanceof StringLiteralExpression) {
            return ((StringLiteralExpression) modelElement).getContents();
        }

        throw new Exception("Invalid argument for model instantiator method");
    }
}
