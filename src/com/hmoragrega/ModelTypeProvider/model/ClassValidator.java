package com.hmoragrega.ModelTypeProvider.model;

import com.jetbrains.php.lang.psi.elements.PhpClass;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validates Model classes
 */
public class ClassValidator {

    private static final Pattern CORE_MODEL_FQN_REGEX = Pattern.compile("\\\\Softonic\\\\Core3\\\\(Model|Config|Controller)");

    /**
     * Searches recursively on the class hierarchy to determine if the class extends Model
     *
     * @param modelClass The class to validate
     * @throws Exception When the class doesn't extend Model
     */
    public void validate(PhpClass modelClass) throws Exception {
        String fqn = modelClass.getFQN();
        if (null != fqn && CORE_MODEL_FQN_REGEX.matcher(fqn).find()) {
            return;
        }

        PhpClass parent = modelClass.getSuperClass();
        if (null == parent) {
            throw new Exception("It doesn't extend Core3, Config or Controller");
        }

        validate(parent);
    }

}
