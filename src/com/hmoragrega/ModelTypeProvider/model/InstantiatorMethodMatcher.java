package com.hmoragrega.ModelTypeProvider.model;

import com.jetbrains.php.lang.psi.elements.MethodReference;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validates that method calls are Model instantiator method
 */
public class InstantiatorMethodMatcher {

    private final static Pattern METHOD_PATTERN = Pattern.compile("(\\\\Softonic\\\\(Core3|\\w+Instance)\\\\.+)(\\.getModelProxy|\\.getClass)$");

    /**
     * Captures the method caller.
     *
     * @param methodReference the method to match
     * @return The method caller fqn
     * @throws Exception
     */
    public String getCallerFqn(MethodReference methodReference) throws Exception
    {
        String methodSignature = methodReference.getSignature();
        Matcher methodMatcher = METHOD_PATTERN.matcher(methodSignature);
        if (!methodMatcher.find()) {
            throw new Exception("Methods does not meet the model caller signature");
        }

        return methodMatcher.group(1);
    }
}
