package com.hmoragrega.ModelTypeProvider.instance;

import com.intellij.psi.PsiElement;
import com.jetbrains.php.lang.psi.PhpFile;
import com.jetbrains.php.lang.psi.elements.Variable;
import com.jetbrains.php.lang.psi.elements.Statement;
import com.jetbrains.php.lang.psi.elements.PhpPsiElement;
import com.jetbrains.php.lang.psi.elements.GroupStatement;
import com.jetbrains.php.lang.psi.elements.ArrayHashElement;
import com.jetbrains.php.lang.psi.elements.AssignmentExpression;
import com.jetbrains.php.lang.psi.elements.ArrayCreationExpression;
import com.jetbrains.php.lang.psi.elements.StringLiteralExpression;

/**
 * Parse instance config files
 */
public class ConfigParser {

    private final static String CONFIG_VARIABLE_NAME = "config";
    private final static String INSTANCE_PARENT_KEY = "parent";

    /**
     * Captures the parent instance for a given instance config file
     *
     * @param configFile Instance configuration file
     * @return the parent instance name
     * @throws Exception When the parent instance could not be extracted
     */
    public String getParent(PhpFile configFile) throws Exception {
        PsiElement groupStatement = configFile.getFirstPsiChild();
        if (!(groupStatement instanceof GroupStatement)) {
            throw new Exception("The config file does not follow the standard");
        }

        return parseGroupStatement((GroupStatement) groupStatement);
    }

    /**
     * Parses the PHP code from the instance config file
     *
     * @param groupStatement Group of PHP statements
     * @return The parent instance name
     * @throws Exception When the parsing goes wrong
     */
    private String parseGroupStatement(GroupStatement groupStatement) throws Exception {
        for (PsiElement statement : groupStatement.getStatements()) {

            if (!(statement instanceof Statement)) {
                continue;
            }

            PhpPsiElement assignment = ((Statement) statement).getFirstPsiChild();
            if (!(assignment instanceof AssignmentExpression)) {
                continue;
            }

            PhpPsiElement variable = ((AssignmentExpression) assignment).getVariable();
            if (!(variable instanceof Variable)) {
                continue;
            }

            String variableName = variable.getName();
            if (null == variableName || !variableName.equals(CONFIG_VARIABLE_NAME)) {
                continue;
            }

            PhpPsiElement values = ((AssignmentExpression) assignment).getValue();
            if (!(values instanceof ArrayCreationExpression)) {
                continue;
            }

            return getInstanceParentFromConfigArray((ArrayCreationExpression) values);
        }

        throw new Exception("The config array assignment expression could not be detected");
    }

    /**
     * Captures the instance name and parent from the config array values
     *
     * @param config The instance $config array creation php code
     * @return the parent instance name
     * @throws RuntimeException When the parent couldn't be extracted from the config array
     */
    private String getInstanceParentFromConfigArray(ArrayCreationExpression config) throws Exception {
        for (ArrayHashElement hashElement : config.getHashElements()) {
            PhpPsiElement key = hashElement.getKey();
            PhpPsiElement value = hashElement.getValue();

            if (!(key instanceof StringLiteralExpression) || !(value instanceof StringLiteralExpression)) {
                continue;
            }

            String keyTrimmed = ((StringLiteralExpression) key).getContents();
            if (keyTrimmed.equals(INSTANCE_PARENT_KEY)) {
                return ((StringLiteralExpression) value).getContents();
            }
        }

        throw new Exception("The parent instance couldn't be extracted from the config");
    }
}
