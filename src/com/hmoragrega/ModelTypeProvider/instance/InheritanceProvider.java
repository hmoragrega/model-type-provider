package com.hmoragrega.ModelTypeProvider.instance;

import com.hmoragrega.ModelTypeProvider.project.Navigator;

import com.jetbrains.php.lang.psi.PhpFile;
import com.jetbrains.php.lang.psi.elements.MethodReference;
import com.jetbrains.php.lang.psi.elements.PhpPsiElement;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

/**
 * Calculates the instance inheritance
 */
public class InheritanceProvider {

    private ConfigParser configParser;
    private Navigator navigator;
    private Matcher instanceMatcher;
    private Map<String, List<String>> instanceInheritanceCache = new HashMap<String, List<String>>();

    /**
     * Inheritance provider constructor
     *
     * @param configParser Instance config files parser
     * @param navigator    Project navigator
     */
    public InheritanceProvider(ConfigParser configParser, Navigator navigator, Matcher instanceMatcher) {
        this.configParser = configParser;
        this.navigator = navigator;
        this.instanceMatcher = instanceMatcher;
    }

    /**
     * Gets the instance inheritance list for a given instance name, calculated by one PHP PSI element
     * Uses cache to not repeating the same searches
     *
     * @param methodReference Method element to use start navigating the project files
     * @return List of inherited instances
     * @throws Exception When the list couldn't be calculated
     */
    public List<String> getInstanceInheritance(MethodReference methodReference) throws Exception {
        List<String> instances = new ArrayList<String>();

        String instanceName = instanceMatcher.getNormalizedFromNamespace(methodReference.getNamespaceName());
        if (null == instanceName) {
            return instances;
        }

        if (instanceInheritanceCache.containsKey(instanceName)) {
            return instanceInheritanceCache.get(instanceName);
        }

        navigator.setInstancesDirectory(methodReference);
        addParentInstances(instances, instanceName);

        instanceInheritanceCache.put(instanceName, instances);

        return instances;
    }

    /**
     * Adds a instance to the list and keeps getting instances until the root instance
     *
     * @param instances    Instances list
     * @param instanceName Instance name to get the parent
     * @throws Exception When some error arises calculating the list
     */
    private void addParentInstances(List<String> instances, String instanceName) throws Exception {
        PhpFile configFile = navigator.getInstanceConfig(instanceName);
        String parent = configParser.getParent(configFile);

        instances.add(instanceName);
        if (!parent.equals(instanceName)) {
            addParentInstances(instances, parent);
        }
    }
}
