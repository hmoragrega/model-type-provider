package com.hmoragrega.ModelTypeProvider;

import com.hmoragrega.ModelTypeProvider.instance.ConfigParser;
import com.hmoragrega.ModelTypeProvider.instance.InheritanceProvider;
import com.hmoragrega.ModelTypeProvider.instance.Matcher;
import com.hmoragrega.ModelTypeProvider.model.ClassValidator;
import com.hmoragrega.ModelTypeProvider.model.InstantiatorMethodMatcher;
import com.hmoragrega.ModelTypeProvider.model.NameFinder;
import com.hmoragrega.ModelTypeProvider.php.ClassFinder;
import com.hmoragrega.ModelTypeProvider.php.ClassParser;
import com.hmoragrega.ModelTypeProvider.php.MethodReferenceValidator;
import com.hmoragrega.ModelTypeProvider.project.Navigator;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.jetbrains.php.lang.psi.elements.*;
import com.jetbrains.php.lang.psi.resolve.types.PhpTypeProvider2;

import java.util.Collection;
import java.util.List;


/**
 * Provider the class type for Softonic Model class for getClass and getModelProxy
 */
public class ModelTypeProvider implements PhpTypeProvider2 {

    private static final char PLUGIN_IDENTIFIER_KEY = 'ω';
    private InstantiatorMethodMatcher methodMatcher = new InstantiatorMethodMatcher();
    private Matcher instanceMatcher = new Matcher();
    private ClassValidator modelClassValidator = new ClassValidator();
    private ClassParser classParser = new ClassParser();
    private ClassFinder classFinder = new ClassFinder();
    private MethodReferenceValidator methodReferenceValidator = new MethodReferenceValidator();
    private ConfigParser configParser = new ConfigParser();
    private Navigator projectNavigator = new Navigator();
    private InheritanceProvider inheritanceProvider = new InheritanceProvider(configParser, projectNavigator, instanceMatcher);
    private NameFinder modelClassNameFinder = new NameFinder(classFinder, classParser);

    /**
     * Returns the plugin identifier key
     */
    @Override
    public char getKey() {
        return PLUGIN_IDENTIFIER_KEY;
    }

    /**
     * Returns the custom signature for Models instantiated through getClass or getModelProxy
     *
     * @param psiElement Element to use to determine the class type
     * @return The plugin custom class signature, null if couldn't be determined
     */
    public String getType(PsiElement psiElement) {
        try {

            MethodReference methodReference = methodReferenceValidator.validate(psiElement);
            String callerFqn = methodMatcher.getCallerFqn(methodReference);

            Project project = methodReference.getProject();
            PhpClass callerClass = classFinder.getClass(project, callerFqn);

            modelClassValidator.validate(callerClass);
            String modelPath = modelClassNameFinder.getModelPath(methodReference);

            List<String> instanceInheritance = inheritanceProvider.getInstanceInheritance(methodReference);
            PhpClass phpClass = classFinder.getClassInInstances(project, modelPath, instanceInheritance);

            return PLUGIN_IDENTIFIER_KEY + phpClass.getFQN();

        } catch (Exception exception) {
            return null;
        }
    }

    /**
     * Returns the collection of classes that match the plugin custom signature
     *
     * @param signature Signature to match
     * @param project   The IDE project
     * @return The classes found by the signature, null if signature is not correct
     */
    @Override
    public Collection<? extends PhpNamedElement> getBySignature(String signature, Project project) {
        if (0 != signature.indexOf(PLUGIN_IDENTIFIER_KEY)) {
            return null;
        }

        return classFinder.getClasses(project, signature.substring(1));
    }
}