package com.hmoragrega.ModelTypeProvider.php;

import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.PsiElement;
import com.jetbrains.php.lang.parser.PhpElementTypes;
import com.jetbrains.php.lang.psi.elements.MethodReference;

/**
 * Validates that PHP elements are method
 */
public class MethodReferenceValidator {

    /**
     * Validates that the given element represent a method reference.
     *
     * @param psiElement Element to validate
     * @throws Exception When the element is not a valid method reference
     */
    public MethodReference validate(PsiElement psiElement) throws Exception {
        if (!PlatformPatterns.psiElement(PhpElementTypes.METHOD_REFERENCE).accepts(psiElement)) {
            throw new Exception("Not a valid method reference element");
        }

        return (MethodReference) psiElement;
    }
}
