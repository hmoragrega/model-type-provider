package com.hmoragrega.ModelTypeProvider.php;

import com.google.common.base.CaseFormat;
import com.intellij.openapi.project.Project;
import com.jetbrains.php.PhpIndex;
import com.jetbrains.php.lang.psi.elements.PhpClass;

import java.util.Collection;
import java.util.List;

/**
 * Allows the search of PHP classes
 */
public class ClassFinder {

    private final static String MODEL_NAMESPACE_PREFIX = "\\Softonic\\";
    private final static String INSTANCE_NAMESPACE_SUFFIX = "Instance\\";
    private final static String CORE_NAMESPACE = "Core3\\";

    /**
     * Get a class by their FQN only when the match is perfect
     *
     * @param project  The project instance
     * @param classFqn Class fully qualified name.
     * @return The requested class
     * @throws Exception When the class couldn't be found or multiple matches are found
     */
    public PhpClass getClass(Project project, String classFqn) throws Exception {
        Collection<PhpClass> classesByFqn = getClasses(project, classFqn);
        if (1 != classesByFqn.size()) {
            throw new Exception("Impossible to determine the class by the FQN");
        }

        return classesByFqn.iterator().next();
    }

    /**
     * Returns a collection of classes based of the FQN
     *
     * @param project  The project instance
     * @param classFqn Class fully qualified name.
     * @return The found classes.
     */
    public Collection<PhpClass> getClasses(Project project, String classFqn) {
        return PhpIndex.getInstance(project).getClassesByFQN(classFqn);
    }

    /**
     * Tries to find the class on the given instances and in core classes
     *
     * @param project   The project instance
     * @param modelPath Relative model path
     * @param instances List of instances name ordered by priority
     * @return The model class
     * @throws Exception When the class couldn't be found
     */
    public PhpClass getClassInInstances(Project project, String modelPath, List<String> instances) throws Exception {
        for (String instance : instances) {
            String modelFqn = getInstanceModelFqn(instance, modelPath);
            Collection<PhpClass> classes = getClasses(project, modelFqn);
            if (1 == classes.size()) {
                return classes.iterator().next();
            }
        }

        return getCoreModelClass(project, modelPath);
    }

    /**
     * Returns a Core3 model class
     *
     * @param project   The project instance
     * @param modelPath Relative model path
     * @return The Core3 model class
     * @throws Exception When the class couldn't be found
     */
    private PhpClass getCoreModelClass(Project project, String modelPath) throws Exception {
        String coreModelFqn = MODEL_NAMESPACE_PREFIX + CORE_NAMESPACE + modelPath;
        return getClass(project, coreModelFqn);
    }

    /**
     * Returns the FQN of an instance model class based on the instance name and model path.
     *
     * @param instance  Instance name
     * @param modelPath Relative model path
     * @return The class FQN
     */
    private String getInstanceModelFqn(String instance, String modelPath) {
        return MODEL_NAMESPACE_PREFIX
                + CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, instance)
                + INSTANCE_NAMESPACE_SUFFIX
                + modelPath;
    }
}
