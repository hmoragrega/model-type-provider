package com.hmoragrega.ModelTypeProvider.php;

import com.intellij.psi.PsiElement;
import com.jetbrains.php.lang.psi.elements.ClassConstantReference;
import com.jetbrains.php.lang.psi.elements.Field;
import com.jetbrains.php.lang.psi.elements.PhpClass;
import com.jetbrains.php.lang.psi.elements.StringLiteralExpression;

import java.util.Collection;

/**
 * Parses PHP class files
 */
public class ClassParser {

    private final static String CLASS_CONSTANT_SIGNATURE_SPLITTER = "((#*)K#C|\\.|\\|\\?)";
    private final static int VALID_CONSTANT_PARTS = 3;
    private final static int CONSTANT_CLASS_PART = 1;
    private final static int CONSTANT_NAME_PART = 2;

    /**
     * Gets the value of a class constant
     *
     * @param constant    Constant PHP reference
     * @param classFinder Class finder service
     * @return The value of the constant
     * @throws Exception When the constant value couldn't be determined
     */
    public String getClassConstantValue(ClassConstantReference constant, ClassFinder classFinder) throws Exception {
        String classConstant = constant.getSignature();
        String[] constantParts = classConstant.split(CLASS_CONSTANT_SIGNATURE_SPLITTER);
        if (VALID_CONSTANT_PARTS != constantParts.length) {
            throw new Exception("Not a valid class constant");
        }

        String className = constantParts[CONSTANT_CLASS_PART];
        String constantName = constantParts[CONSTANT_NAME_PART];

        PhpClass constantClass = classFinder.getClass(constant.getProject(), className);

        return getConstantValueFromClass(constantName, constantClass);
    }

    /**
     * Gets the constant value from a PHP class
     *
     * @param constantName Name of the constant
     * @param phpClass     Class to search in
     * @return The constant value
     * @throws Exception When the constant value couldn't be captured
     */
    private String getConstantValueFromClass(String constantName, PhpClass phpClass) throws Exception {
        Collection<Field> fields = phpClass.getFields();
        for (Field field : fields) {
            if (field.isConstant() && field.getName().equals(constantName)) {
                PsiElement constantValue = field.getDefaultValue();
                if (null == constantValue || !(constantValue instanceof StringLiteralExpression)) {
                    return null;
                }

                return ((StringLiteralExpression) constantValue).getContents();
            }
        }

        throw new Exception("Could not get class constant value");
    }
}
