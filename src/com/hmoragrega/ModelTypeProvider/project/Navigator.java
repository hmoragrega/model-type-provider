package com.hmoragrega.ModelTypeProvider.project;

import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiDirectory;
import com.jetbrains.php.lang.psi.PhpFile;
import com.jetbrains.php.lang.psi.elements.PhpPsiElement;

/**
 * Navigates the project structure
 */
public class Navigator {

    private final static String INSTANCES_DIR_NAME = "instances";
    private final static String CONFIG_DIR_NAME = "config";
    private final static String INSTANCE_CONFIG_FILENAME = "instances.config.php";

    private PsiDirectory instancesDirectory;

    /**
     * Sets the instance directory based on the PhpPsiElement location
     *
     * @param phpPsiElement The element used to determine the instances directory
     * @throws Exception When
     */
    public void setInstancesDirectory(PhpPsiElement phpPsiElement) throws Exception {
        this.instancesDirectory = getInstancesDirectory(phpPsiElement.getContainingFile().getContainingDirectory());
    }

    /**
     * Keeps navigating directories up in the project tree until it finds the instances directory
     *
     * @param directory Directory to check
     * @return The instances directory
     * @throws Exception When the instances directory could not be detected
     */
    private PsiDirectory getInstancesDirectory(PsiDirectory directory) throws Exception {
        if (null == directory) {
            throw new Exception("Could not get instances directory");
        }

        if (directory.getName().equals(INSTANCES_DIR_NAME)) {
            return directory;
        }

        return getInstancesDirectory(directory.getParentDirectory());
    }

    /**
     * Gets a subdirectory
     *
     * @param directory        Directory to search in
     * @param subdirectoryName Subdirectory we are searching for
     * @return The subdirectory element
     * @throws Exception When the subdirectory couldn't be found
     */
    private PsiDirectory getSubdirectory(PsiDirectory directory, String subdirectoryName) throws Exception {
        PsiDirectory subdirectory = directory.findSubdirectory(subdirectoryName);
        if (null == subdirectory) {
            throw new Exception("Cannot find subdirectory " + subdirectoryName);
        }

        return subdirectory;
    }

    /**
     * @param instanceName
     * @return
     * @throws Exception
     */
    public PhpFile getInstanceConfig(String instanceName) throws Exception {
        PsiDirectory instanceDirectory = getSubdirectory(instancesDirectory, instanceName);
        PsiDirectory configDirectory = instanceDirectory.findSubdirectory(CONFIG_DIR_NAME);

        return (PhpFile) getFile(configDirectory, INSTANCE_CONFIG_FILENAME);
    }

    private PsiFile getFile(PsiDirectory directory, String filename) throws Exception {
        PsiFile file = directory.findFile(filename);
        if (null == file) {
            throw new Exception("Could not find file " + filename);
        }

        return file;
    }
}
